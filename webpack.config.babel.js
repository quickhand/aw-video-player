import webpack from "webpack";
    
export default [{
    entry: {
        "aw-video-player": __dirname + "/src/entry-commonjs.js",
        "aw-video-player-global": __dirname + "/src/entry-global.js"
    },
    output: {
        path: __dirname + "/dist/",
        filename: "[name].js",
        library: "aw-video-player",
        libraryTarget: 'umd'
    },
    module: {
        loaders: [
            {
                test: /\.js.?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    presets: ['es2015']
                }
            }
        ]
    }
}];


