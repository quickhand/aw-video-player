export default function(element,options) {
    
    if(typeof options ==="undefined") {
        options = {};
    }
    let dodashconfig=false;
    if(window && window.MediaSource && window.Dash && window.MediaPlayer && 'dash' in options) { 
        let sourceel = document.createElement('source');
        sourceel.src=options.dash;
        sourceel.type="application/dash+xml";
        element.appendChild(sourceel);
        dodashconfig = true;
    }
    if('hls' in options) {
        let sourceel = document.createElement('source');
        sourceel.src=options.hls;
        sourceel.type="application/x-mpegURL";
        element.appendChild(sourceel);
    }
    if('mp4' in options) {
        let sourceel = document.createElement('source');
        sourceel.src=options.mp4;
        sourceel.type="video/mp4";
        element.appendChild(sourceel);
    }
    if('webm' in options) {
        let sourceel = document.createElement('source');
        sourceel.src=options.webm;
        sourceel.type="video/webm";
        element.appendChild(sourceel);
    }
    if(dodashconfig) {
        let context = new Dash.di.DashContext();
        let player = new MediaPlayer(context);
        player.startup();
        player.attachView(element);
        player.attachSource(options.dash);
    }

} 